package lab6Example;

public class Main {

    public static void main(String[] args) {

        
        Bshop shop = new Bshop();
        Barber barber = new Barber(shop);
        CustomerGenerator cg = new CustomerGenerator(shop);
        Thread thbarber = new Thread(barber);
        Thread thcg = new Thread(cg);
        Clock clock = new Clock(barber,cg);
        clock.start();
        thcg.start();
        thbarber.start();

    }

}
