/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6Example;

/**
 *
 * @author kklee998
 */
public class Clock extends Thread{
    
    Barber barber;
    CustomerGenerator cg;
    Thread counter;
    int i = 0;
    
    public Clock(Barber barber, CustomerGenerator cg) {
        this.barber = barber;
        this.cg = cg;
    }
    
    public void start(){
        counter = new Thread (this);
        counter.start();
        System.out.println("START THE CLOCK\n\n");
    }
    
    
    public void run() {
        while(true){
            if (counter == null) {
                return;
            }
            if (i < 15) {
                System.out.println("CLOCK IS NOW:" + i + "\n\n");
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e) {
                    
                }
                i++;
            }
            if (i == 15){
                System.out.println("CLOCK: IT'S CLOSING TIME!\n\n");
                barber.setclosingTime();
                cg.setclosingTime();
                return;
            }
        }
    }
    
    
}
