/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labexample;

/**
 *
 * @author kklee998
 */
class RunnableThread implements Runnable {

    private Thread t;
    private String threadName;

    RunnableThread(String name) {
        threadName = name;
        System.out.println("Creating: " + threadName);
    }

    public void run() {
        System.out.println("Running: " + threadName);
        try {
            for (int i = 4; i > 0; i--) {
                System.out.println("Thread: " + threadName + ", Loop: " + i);
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread: " + threadName + "Interrupted");

        }
        System.out.println("Thread: " + threadName + " exiting");
    }
    
    public void start(){
        System.out.println("Starting: " + threadName);
        if (t == null) {
            t = new Thread (this,threadName);
            t.start();
        }
    }

}
