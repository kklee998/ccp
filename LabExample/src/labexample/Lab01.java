/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labexample;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author kklee998
 */
public class Lab01 extends JPanel{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //B1();
        B2();

    }

    public static void B1() {

        int i = 0;
        int x;
        float y = 0;

        //solution taken from:https://stackoverflow.com/questions/5287538/how-can-i-get-the-user-input-in-java
        Scanner reader = new Scanner(System.in);
        System.out.println("How many numbers do you wish to add: ");
        int n = reader.nextInt();

        while (i < n) {
            Scanner reader2 = new Scanner(System.in);
            System.out.println("Enter a number: ");
            x = reader2.nextInt();
            y = x + y;
            i += 1;

        }

        System.out.println("\nSum of all the numbers is:" + y);
        System.out.println("\nAverage of all the number is " + y / n);
        reader.close();
    }

    public void paint(Graphics g) {

        //solution from: https://www.tutorialspoint.com/javaexamples/gui_rectangle.htm
        g.setFont(new Font("", 0, 100));
        FontMetrics fm = getFontMetrics(new Font("", 0, 100));
        String s = "I don't read instructions well";
        int x = 5;
        int y = 5;

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int h = fm.getHeight();
            int w = fm.charWidth(c);

            g.drawRect(x, y, w, h);
            g.drawString(String.valueOf(c), x, y + h);
            x = x + w;

        }

    }

    public static void B2() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(new Lab01());
        frame.setSize(500, 700);
        frame.setVisible(true);
    }
}
